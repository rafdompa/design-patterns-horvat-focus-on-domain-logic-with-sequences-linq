﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyIterator.Utils
{
    public static class EnumerableExtensions
    {
        public static T WithMinimum<T, TKey>(this IEnumerable<T> sequence, Func<T, TKey> criterion)
            where T : class
            where TKey : IComparable<TKey>
        {
            if (sequence.Count() == 0)
                return null;

            return sequence
                .Select(obj => Tuple.Create(obj, criterion(obj))) //create sequence with calculated prices; placed as <painter,calculatedPrice> tuple
                .Aggregate( //no need to seed aggregate with null tuple
                (best, curr) => 
                curr.Item2.CompareTo(best.Item2) < 0 ? curr : best)  //use the already calculated prices to compare and pick cheapest
                .Item1; //select only 'T' out of the tuple (the painter object)
        }
    }
}
