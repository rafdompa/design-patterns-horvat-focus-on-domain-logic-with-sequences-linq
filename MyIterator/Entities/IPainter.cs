﻿using System;

namespace MyIterator.Entities
{
    public interface IPainter
    {
        string Name { get; }
        bool IsAvailable { get; set; }
        TimeSpan EstimateTimeToPaint(double sqMeters);
        Decimal EstimateCompensation(double sqMeters);
    }
}
