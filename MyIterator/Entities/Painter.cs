﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyIterator.Entities
{
    public class Painter : IPainter
    {
        public string Name { get; private set; }
        public bool IsAvailable { get; set; }
        private decimal pricePerSqMeter { get; set; }
        private decimal jobFee { get; set; }
        private TimeSpan ratePerSqMeter { get; set; }

        public Painter(string name, Decimal pricePerSqMeter, Decimal jobFee, 
            TimeSpan ratePerSqMeter, bool currenlyAvailable)
        {
            this.Name = name;
            this.pricePerSqMeter = pricePerSqMeter;
            this.jobFee = jobFee;
            this.ratePerSqMeter = ratePerSqMeter;
            this.IsAvailable = currenlyAvailable;
        }
        

        public Decimal EstimateCompensation(double sqMeters) => 
            jobFee + ((Decimal)sqMeters * pricePerSqMeter);

        public TimeSpan EstimateTimeToPaint(double sqMeters) => 
            ratePerSqMeter * sqMeters;
    }
}
