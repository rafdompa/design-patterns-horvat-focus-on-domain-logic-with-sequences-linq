﻿using System;
using System.Collections.Generic;
using MyIterator.Entities;
using MyIterator.Utils;
using System.Linq;

namespace MyIterator
{
    public class BusinessLogic
    {
        public static IPainter FindCheapestPainter(double sqMeters, IEnumerable<IPainter> painters)
        {
            return painters
                .Where(painter => painter.IsAvailable)
                .WithMinimum(painter => painter.EstimateCompensation(sqMeters));   
        }
    }
}
