using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyIterator.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyIterator.Tests
{
    [TestClass]
    public class BusinessLogicTest
    {
        IEnumerable<IPainter> painters;

        [TestInitialize]
        public void Init()
        { 
            painters = new List<IPainter>()
            {
                new Painter(
                    name: "P1",
                    pricePerSqMeter: 10.50M,
                    jobFee: 150M,
                    ratePerSqMeter: new TimeSpan(0, 30, 0),
                    currenlyAvailable: true
                ),
                new Painter(
                    name: "P2",
                    pricePerSqMeter: 8.00M,
                    jobFee: 100M,
                    ratePerSqMeter: new TimeSpan(0, 45, 0),
                    currenlyAvailable: true
                ),
                new Painter(
                    name: "P3",
                    pricePerSqMeter: 5.00M,
                    jobFee: 50,
                    ratePerSqMeter: new TimeSpan(0, 45, 0),
                    currenlyAvailable: false
                ),
                new Painter(
                    name: "P4",
                    pricePerSqMeter: 8.50M,
                    jobFee: 1000M,
                    ratePerSqMeter: new TimeSpan(0, 15, 0),
                    currenlyAvailable: true
                ),

            };
        }

        [TestMethod]
        public void FindCheapestPainter_Test()
        {
            var cheapest = BusinessLogic.FindCheapestPainter(1000, painters);

            Assert.AreEqual("P2", cheapest.Name);
        }

        [TestMethod]
        public void FindCheapestPainter_None_Available_Test()
        {
            var unavailablePainters = painters.Select(
                    x => { x.IsAvailable = false; return x; }
                );

            var cheapest = BusinessLogic.FindCheapestPainter(1000, unavailablePainters);

            Assert.IsNull(cheapest);
        }

        [TestMethod]
        public void FindCheapestPainter_Single_Painter_Test()
        {
            var singlePainter = painters.Take(1);

            var cheapest = BusinessLogic.FindCheapestPainter(1000, singlePainter);

            Assert.AreEqual("P1", cheapest.Name);
        }
    }
}